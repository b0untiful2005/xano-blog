import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from '../config.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	public config: XanoConfig;
	public configured: boolean = false;

	constructor(
		private configService: ConfigService
	) {
	}

	ngOnInit(): void {
		this.config = this.configService.config;
		this.configService.xanoApiUrl.subscribe(apiUrl => this.configured = !!apiUrl);
	}

}
